#ifndef MTF_SSD_H
#define MTF_SSD_H

#include "SSDBase.h"

_MTF_BEGIN_NAMESPACE


class SSD : public SSDBase{
public:

	typedef AMParams ParamType;
	ParamType params;

	SSD(const ParamType *ssd_params = nullptr, const int _n_channels = 1);
	double getLikelihood() const override{
		return exp(-params.likelihood_alpha * sqrt(-f / (static_cast<double>(patch_size))));
	}
	double operator()(const double* a, const double* b,
		size_t size, double worst_dist=-1) const override{
		double dist = SSDBase::operator()(a, b, size, worst_dist);
		return params.dist_from_likelihood ?
			-exp(-params.likelihood_alpha * sqrt( dist / (static_cast<double>(patch_size)))) : dist;
	}
};

_MTF_END_NAMESPACE

#endif